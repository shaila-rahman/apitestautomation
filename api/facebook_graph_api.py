import json
import requests

import settings


class FacebookGraphApi(object):

    access_token = settings.FB_ACCESS_TOKEN
    host = settings.FB_GRAPH_API_HOST

    def get_page_posts(self, page_id):
        params = {
            "access_token": self.access_token,
        }

        end_point = "{}/{}/posts".format(self.host, page_id)

        response_data = requests.get(end_point, params=params)

        # import pdb; pdb.set_trace()

        assert response_data.status_code == 200

        return json.loads(response_data.text)


