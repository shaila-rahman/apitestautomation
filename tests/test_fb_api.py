from tests.base import ApiAutomationBase
from api.facebook_graph_api import FacebookGraphApi


class FacebookApiTest(ApiAutomationBase):

    facebook_api = FacebookGraphApi()

    def test_page_posts(self):
        page_id = 'eduhive'
        page_posts = self.facebook_api.get_page_posts(page_id)['data']

        assert len(page_posts) > 0
        print(page_posts[0])

        # import pdb; pdb.set_trace()
